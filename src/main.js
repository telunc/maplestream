/**
* Discord Bot Neinheart
*/

// config
const config = require('config');
const discordToken = config.get('discord.token');

// discord
const Discord = require('discord.js');
global.client = new Discord.Client();
const Mio = require('./mio.js');
let mio = new Mio();
let channels = [];

let daily_timer, socket, broadcast;

// Discord
client.on('ready', () => {
    console.log(`Logged in as ${client.user.username}!`);
	console.log(`Number of users: ${client.users.size}!`);
	
	// initiate broadcast
	if (!broadcast){
		broadcastCreate();
	}

	streamInit();
});

function broadcastCreate(){
	broadcast = client.createVoiceBroadcast();
	broadcast.on('end', ()=>{
		mio.update();
		console.log('Broadcast ended');
		setTimeout(streamInit, 500); // 0.5 sec delay
	});
	broadcast.on('error', (err)=>{
		console.log('Broadcast error: '+err);
		streamInit();
	});
}

client.on('message', (message)=>{
	if (!message.member) return null;
	if (message.content==='/join'){
		let channel = channels[message.guild.id];
		let voiceChannel = message.member.voiceChannel;
		if (!channel){
            if (!voiceChannel) return message.reply('you are not in a voice channel');
            channel = channels[message.guild.id] = {
                "voiceChannel": voiceChannel
            };
            voiceChannel.join().then((connection) => {
                channel.connection = connection;
				playBroadcast(channel);
            } ).catch((err) => {
                // voice connection error
				console.log(err);
                message.channel.send('Failed to Join Voice Channel');
            });
        }
	} else if(message.content==='/leave'){
		let channel = channels[message.guild.id];
		if (channel){
			channel.connection = null;
			if (channel.dispatcher) channel.dispatcher.end();
			if (channel.voiceChannel) channel.voiceChannel.leave();
			delete channels[message.guild.id];
		}
	}
});

// Add music to broadcast
function streamInit(){
	mio.get(function(err, url){
		console.log(url);
		if (!err){
			broadcast.playArbitraryInput(url); 
		} else{
			console.log(err);
		}
	});
}

// Play broadcast
function playBroadcast(channel){
	let dispatcher = channel.connection.playBroadcast(broadcast);
	channel.dispatcher = dispatcher;
	dispatcher.on('end', ()=>{
		console.log('Dispatcher ended');
		dispatcher = null;
		channel.dispatcher = null;
		if (channel.connection) {
			console.log('Trying to replay');
			playBroadcast(channel);
		} else{
			console.log('connection lost');
		}
	});
	dispatcher.on('error', ()=>{
		console.log('Dispatcher error: '+err);
		dispatcher = null;
		channel.dispatcher = null;
		if (connection){
			console.log('Trying to replay');
			playBroadcast(channel);
		} else{
			console.log('connection lost');
		}
	});
}

client.login(discordToken);
