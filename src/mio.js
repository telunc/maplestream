// pull music from m.io

const request = require('request');

let queueIndex = 0;
let queue; 
let current;

class Mio{
    musics(callback){
        if (!queue){
        request({url: 'http://labs.maplestory.io/api/music', json: true}, function(error, response, body) {
            if (!error){
                queue = body.filter(function(key){
                    if (key.includes('bgm')) {
                        return key;
                    }
                });
                callback(null, queue);
            } else{
                callback(error, null);
            }
        });
        } else{
            callback(null, queue);
        }
    }

    update(){
        if (queueIndex+1 === queue.length){
            queueIndex = 0;
        } else{
            queueIndex++;
        }
    }

    get(callback){
        this.musics(function(error, results){
            if (!error){
                current = music(results[queueIndex]);
                callback(null, current);
            } else{
                console.log('failed to get music');
                callback(error, null);
            }
        });
    }
}

function music(url){
    return 'http://labs.maplestory.io/api/music/' + url;
}

module.exports = Mio;

