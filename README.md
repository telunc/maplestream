# MapleStream

**MapleStream** - is a Discord bot that uses MapleStory.io API.

### How to Set up

`npm install`  
`npm start`

### Invite MapleStream

[https://discordapp.com/oauth2/authorize?&client_id=318662055324286976&scope=bot&permissions=305196094](https://discordapp.com/oauth2/authorize?&client_id=318662055324286976&scope=bot&permissions=305196094)

### Commands
/join    join voice channel  
/leave   leave voice channel